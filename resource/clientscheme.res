//
// TRACKER SCHEME RESOURCE FILE
//
// sections:
//		colors			- all the colors used by the scheme
//		basesettings	- contains settings for app to use to draw controls
//		fonts			- list of all the fonts used by app
//		borders			- description of all the borders
//
//

#base "scheme/Colors.res"

#base "scheme/BaseSettings.res"

#base "scheme/Fonts.res"

#base "scheme/CustomFonts.res"

#base "scheme/CustomFontFiles.res"

#base "scheme/Crosshairs.res"

#base "scheme/Borders.res"
