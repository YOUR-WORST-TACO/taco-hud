Scheme
{
    Fonts
    {
        TacoDefault
        {
            "1"
            {
                "name"          "Caviar Dreams"
                "tall"          "15"
                "weight"        "500"
                "antialias"     "1"
            }
        }
        TacoDefaultBold
        {
            "1"
            {
                "name"          "Caviar Dreams Bold"
                "tall"          "15"
                "weight"        "500"
                "antialias"     "1"
            }
        }
        TacoLarge
        {
            "1"
            {
                "name"          "Champagne & Limousines Bold"
                "tall"          "40"
                "weight"        "500"
                "antialias"     "1"
            }
        }
        TacoScoreboardKills
        {
            "1"
            {
                "name"          "Champagne & Limousines Bold"
                "tall"          "26"
                "weight"        "500"
                "antialias"     "1"
            }
        }
        TacoHealthAmmo
        {
            "1"
            {
                "name"          "Champagne & Limousines Bold"
                "tall"          "50"
                "weight"        "500"
                "antialias"     "1"
            }
        }
        TacoHealthAmmoSmall
        {
            "1"
            {
                "name"          "Champagne & Limousines Bold"
                "tall"          "30"
                "weight"        "500"
                "antialias"     "1"
            }
        }
        TacoStickies
        {
            "1"
            {
                "name"          "Champagne & Limousines Bold"
                "tall"          "20"
                "weight"        "500"
                "antialias"     "1"
            }
        }
        TacoSmall
        {
            "1"
            {
                "name"          "Champagne & Limousines Bold"
                "tall"          "7"
                "weight"        "500"
                "antialias"     "1"
            }
        }
        TacoCurrency
        {
            "1"
            {
                "name"          "Champagne & Limousines Bold"
                "tall"          "15"
                "weight"        "500"
                "antialias"     "1"
            }
        }
        TacoCurrencySmall
        {
            "1"
            {
                "name"          "Champagne & Limousines Bold"
                "tall"          "10"
                "weight"        "500"
                "antialias"     "1"
            }
        }

        "Class Symbols 40"
		{
			"1"
			{
				"name"			"TF2 Class Icons"
				"tall"			"40"
				"additive"  	"0"
				"antialias"		"1"
			}
		}
		
		"Class Symbols 48"
		{
			"1"
			{
				"name"			"TF2 Class Icons"
				"tall"			"48"
				"additive"  	"0"
				"antialias"		"1"
			}
		}
		
		"Class Symbols 72"
		{
			"1"
			{
				"name"			"TF2 Class Icons"
				"tall"			"72"
				"additive"  	"0"
				"antialias"		"1"
			}
		}
        "Symbols 10"
		{
			"1"
			{
				"name"			"Hypnotize Icons"
				"tall"			"10"
				"additive"  	"0"
				"antialias"		"1"
			}
		}
		
		"Symbols 12"
		{
			"1"
			{
				"name"			"Hypnotize Icons"
				"tall"			"12"
				"additive"  	"0"
				"antialias"		"1"
			}
		}
		
		"Symbols 14"
		{
			"1"
			{
				"name"			"Hypnotize Icons"
				"tall"			"14"
				"additive"  	"0"
				"antialias"		"1"
			}
		}
		
		"Symbols 16"
		{
			"1"
			{
				"name"			"Hypnotize Icons"
				"tall"			"16"
				"additive"  	"0"
				"antialias"		"1"
			}
		}
		
		"Symbols 18"
		{
			"1"
			{
				"name"			"Hypnotize Icons"
				"tall"			"18"
				"additive"  	"0"
				"antialias"		"1"
			}
		}
		
		"Symbols 20"
		{
			"1"
			{
				"name"			"Hypnotize Icons"
				"tall"			"20"
				"additive"  	"0"
				"antialias"		"1"
			}
		}
		
		"Symbols 22"
		{
			"1"
			{
				"name"			"Hypnotize Icons"
				"tall"			"22"
				"additive"  	"0"
				"antialias"		"1"
			}
		}
		
		"Symbols 24"
		{
			"1"
			{
				"name"			"Hypnotize Icons"
				"tall"			"24"
				"additive"  	"0"
				"antialias"		"1"
			}
		}
		
		"Symbols 26"
		{
			"1"
			{
				"name"			"Hypnotize Icons"
				"tall"			"26"
				"additive"  	"0"
				"antialias"		"1"
			}
		}
		
		"Symbols 28"
		{
			"1"
			{
				"name"			"Hypnotize Icons"
				"tall"			"28"
				"additive"  	"0"
				"antialias"		"1"
			}
		}
		
		"Symbols 30"
		{
			"1"
			{
				"name"			"Hypnotize Icons"
				"tall"			"30"
				"additive"  	"0"
				"antialias"		"1"
			}
		}
		
		"Symbols 34"
		{
			"1"
			{
				"name"			"Hypnotize Icons"
				"tall"			"34"
				"additive"  	"0"
				"antialias"		"1"
			}
		}
		
		"Symbols 38"
		{
			"1"
			{
				"name"			"Hypnotize Icons"
				"tall"			"38"
				"additive"  	"0"
				"antialias"		"1"
			}
		}
		
		"Symbols 40"
		{
			"1"
			{
				"name"			"Hypnotize Icons"
				"tall"			"40"
				"additive"  	"0"
				"antialias"		"1"
			}
		}
		
		"Symbols 46"
		{
			"1"
			{
				"name"			"Hypnotize Icons"
				"tall"			"46"
				"additive"  	"0"
				"antialias"		"1"
			}
		}
		
		"Symbols 48"
		{
			"1"
			{
				"name"			"Hypnotize Icons"
				"tall"			"48"
				"additive"  	"0"
				"antialias"		"1"
			}
		}
		
		"Symbols 50"
		{
			"1"
			{
				"name"			"Hypnotize Icons"
				"tall"			"50"
				"additive"  	"0"
				"antialias"		"1"
			}
		}
		
		"Symbols 54"
		{
			"1"
			{
				"name"			"Hypnotize Icons"
				"tall"			"54"
				"additive"  	"0"
				"antialias"		"1"
			}
		}
		
		"Symbols 58"
		{
			"1"
			{
				"name"			"Hypnotize Icons"
				"tall"			"58"
				"additive"  	"0"
				"antialias"		"1"
			}
		}
		
		"Symbols 60"
		{
			"1"
			{
				"name"			"Hypnotize Icons"
				"tall"			"60"
				"additive"  	"0"
				"antialias"		"1"
			}
		}
		
		"Symbols 64"
		{
			"1"
			{
				"name"			"Hypnotize Icons"
				"tall"			"64"
				"additive"  	"0"
				"antialias"		"1"
			}
		}
		
		"Symbols 68"
		{
			"1"
			{
				"name"			"Hypnotize Icons"
				"tall"			"68"
				"additive"  	"0"
				"antialias"		"1"
			}
		}
		
		"Symbols 70"
		{
			"1"
			{
				"name"			"Hypnotize Icons"
				"tall"			"70"
				"additive"  	"0"
				"antialias"		"1"
			}
		}
		
		"Symbols 72"
		{
			"1"
			{
				"name"			"Hypnotize Icons"
				"tall"			"72"
				"additive"  	"0"
				"antialias"		"1"
			}
		}
    }
}